const getSum = (str1, str2) => {
  return [str1, str2].some(
    (el) => typeof el !== "string" || (el != parseInt(el) && el !== "")
  )
    ? false
    : +str1 + +str2 + "";
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let result = "";
  result +=
    "Post:" + listOfPosts.filter((el) => el.author === authorName).length;

  let comments = 0;

  listOfPosts.forEach((el) => {
    if (el.comments) {
      el.comments.forEach((comment) => {
        if (comment.author === authorName) {
          comments++;
        }
      });
    }
  });

  return (result += ",comments:" + comments);
};

const tickets = (people) => {
  people = people.map((el) => +el);
  const price = 25;

  const bills = [...people];
  const box = [];

  for (const bill of people) {
    let sum;

    switch (box.length) {
      case 0:
        sum = 0;
        break;
      case 1:
        sum = box[0];
        break;
      default:
        sum = box.reduce((acc, cur) => acc + cur);
        break;
    }

    if (bill < price) {
      return "NO";
    }

    if (bill === price) {
      box.push(bills.shift());
    }

    if (bill > price) {
      const change = bill - price;

      if (change > sum) {
        return "NO";
      }

      if (box.includes(change)) {
        box.push(bills.shift());
        box.splice(box.indexOf(change), 1);
      } else {
        let toGiveChange = box
          .filter((el) => el < change)
          .sort((a, b) => b - a);

        if (toGiveChange.length === 0) {
          return "NO";
        }

        let changeToBeGiven = 0;
        let rest = change;
        const chargeBills = [];

        for (let i = 0; i < box.length; i++) {
          if (changeToBeGiven === change) break;

          changeToBeGiven += toGiveChange[0];
          chargeBills.push(toGiveChange[0]);

          if (changeToBeGiven !== change) {
            rest -= toGiveChange[0];
            toGiveChange = toGiveChange.filter((el) => el <= rest);

            if (toGiveChange.length === 0) {
              return "NO";
            }
          }
        }

        box.push(bills.shift());
        chargeBills.forEach((el) => box.splice(box.indexOf(el), 1));
      }
    }
  }

  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
